var arrNum = [];

document.querySelector("#btnThemSo").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  for (var index = 0; index < arrNum.length; index++) {
    document.querySelector("#ketQua").innerHTML = arrNum;
  }
};
//1
document.querySelector("#btnSumPositive").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  for (var sum1 = 0, index = 0; index < arrNum.length; index++) {
    if (arrNum[index] > 0) {
      sum1 += arrNum[index];
    }
  }
  document.querySelector("#ketQua1").innerHTML = sum1;
};
//2
document.querySelector("#btnCountPositiveNum").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  arrNum.push(number);
  document.querySelector("#nhapSo").value = "";
  for (var count1 = 0, index = 0; index < arrNum.length; index++) {
    if (arrNum[index] > 0) {
      count1 += 1;
    }
  }
  document.querySelector("#ketQua2").innerHTML = count1;
};
//3
document.querySelector("#btnFindMinNum").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  arrNum.push(number);
  document.querySelector("#nhapSo").value = "";
  var minNum = arrNum[0];
  for (var index = 0; index < arrNum.length; index++) {
    if (minNum > arrNum[index]) {
      minNum = arrNum[index];
    }
  }
  document.querySelector("#ketQua3").innerHTML = minNum;
};
//4
document.querySelector("#btnFindMinPosNum").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  if (number >= 0) {
    arrNum.push(number);
  }
  var minPosNum = arrNum[0];
  for (var index = 0; index < arrNum.length; index++) {
    if (minPosNum > arrNum[index]) {
      minPosNum = arrNum[index];
    }
  }
  document.querySelector("#ketQua4").innerHTML = minPosNum;
};
//5
document.querySelector("#btnFindlastEven").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  if (number != 0) {
    arrNum.push(number);
  }
  for (var ketQua5 = -1, index = 0; index < arrNum.length; index++) {
    if (arrNum[index] % 2 == 0) {
      ketQua5 = arrNum[index];
    }
  }
  document.querySelector("#ketQua5").innerHTML = ketQua5;
};
//6
document.querySelector("#btnSwap").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  var viTriA = document.querySelector("#viTriA").value * 1;
  var viTriB = document.querySelector("#viTriB").value * 1;
  for (var index = 0; index < arrNum.length; index++) {
    var viTriCurrent = arrNum[viTriA];
    arrNum[viTriA] = arrNum[viTriB];
    arrNum[viTriB] = viTriCurrent;
  }
  document.querySelector("#ketQua6").innerHTML = arrNum;
};
//7
document.querySelector("#btnSort").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  for (var index = 0; index < arrNum.length; index++) {
    document.querySelector("#ketQua7").innerHTML = arrNum.sort((a, b) => a - b);
  }
};
//8
document.querySelector("#btnFindPriNum").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  var count = 0;
  var ketQua8 = -1;
  for (var index = 0; index < arrNum.length; index++) {
    for (var i = 2; i <= arrNum[index]; i++) {
      if (arrNum[index] % i == 0 && arrNum[index] > 1) {
        count += 1;
      }
    }
    if (count == 1) {
      ketQua8 = arrNum[index];
    }
  }
  document.querySelector("#ketQua8").innerHTML = ketQua8;
};
//9
document.querySelector("#btnFindPosNum").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  for (var sum = 0, index = 0; index < arrNum.length; index++) {
    if (Math.floor(arrNum[index] / 1) == arrNum[index] / 1) {
      sum += 1;
    }
  }
  document.querySelector("#ketQua9").innerHTML = sum;
};
//10
document.querySelector("#btnCompare").onclick = function () {
  var number = document.querySelector("#nhapSo").value * 1;
  document.querySelector("#nhapSo").value = "";
  arrNum.push(number);
  console.log("arrNum: ", arrNum);
  for (
    var ketQua10 = "", sumPos = 0, sumNega = 0, index = 0;
    index < arrNum.length;
    index++
  ) {
    if (arrNum[index] > 0) {
      sumPos += 1;
    }
    if (arrNum[index] < 0) {
      sumNega += 1;
    }
  }
  if (sumPos - sumNega > 0) {
    ketQua10 = "Số dương nhiều hơn";
  } else if (sumPos - sumNega == 0) {
    ketQua10 = "Số dương = số âm";
  } else {
    ketQua10 = "Số âm nhiều hơn";
  }
  document.querySelector("#ketQua10").innerHTML = ketQua10;
};
